package com.example.ussuario.tallerandroid.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ussuario.tallerandroid.Entidades.Estudiante;
import com.example.ussuario.tallerandroid.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetalleEstudiante.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DetalleEstudiante#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetalleEstudiante extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    TextView nombre,ci,telefono,mail,faltas;
    ImageView img;
    int ciEst, fal;
    Estudiante e = new Estudiante();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public DetalleEstudiante() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetalleEstudiante.
     */
    // TODO: Rename and change types and number of parameters
    public static DetalleEstudiante newInstance(String param1, String param2) {
        DetalleEstudiante fragment = new DetalleEstudiante();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_detalle_estudiante, container, false);
        nombre = (TextView) v.findViewById(R.id.estNombre);
        ci = (TextView) v.findViewById(R.id.estCI);
        telefono = (TextView) v.findViewById(R.id.estTelefono);
        mail = (TextView) v.findViewById(R.id.estMail);
        faltas = (TextView) v.findViewById(R.id.estFaltas);
        img = (ImageView) v.findViewById(R.id.imgEstudiante);
        fal=0;

        ciEst = 0;
        Bundle args = getArguments();
        if(args != null){
            ciEst = args.getInt("ciEstudiante");
        }

        detalleEstudiante de = new detalleEstudiante();
        de.execute();

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class detalleEstudiante extends AsyncTask<Void, Void, Void> {
        ProgressDialog dialog = new ProgressDialog(getActivity());

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("DetalleEstudiante", "doInBackground");
            HttpClient httpClient = new DefaultHttpClient();

            HttpGet get = new HttpGet("http://cristmathbri.webcindario.com/DetalleEstudiante.php?IdEstudiante=" + ciEst);
            get.setHeader("Content-type", "application/json");

            try {
                HttpResponse resp = httpClient.execute(get);
                String respString = EntityUtils.toString(resp.getEntity());
                JSONObject jsonResultado = new JSONObject(respString);
                JSONArray respJSONa = jsonResultado.getJSONArray("Estudiante");
                for (int i = 0; i < respJSONa.length(); i++) {
                    JSONObject jsonObject = respJSONa.getJSONObject(i);

                    int cedula = jsonObject.getInt("cedula");
                    String nom = jsonObject.getString("nombre");
                    int tel = jsonObject.getInt("telefono");
                    String email = jsonObject.getString("mail");
                    String foto = jsonObject.getString("foto");

                    e = new Estudiante(cedula, nom, tel, email, foto);
                }

            } catch (Exception ex) {
                Log.e("ServicioRest", "Error!", ex);
                ex.printStackTrace();
            }

            HttpGet get2 = new HttpGet("http://cristmathbri.webcindario.com/InasistenciasEstudiante.php?IdEstudiante=" + ciEst);
            get2.setHeader("Content-type", "application/json");

            try {
                HttpResponse resp = httpClient.execute(get2);
                String respString = EntityUtils.toString(resp.getEntity());
                JSONObject jsonResultado = new JSONObject(respString);
                fal = jsonResultado.getInt("Faltas");

            } catch (Exception ex) {
                Log.e("ServicioRest", "Error!", ex);
                ex.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPreExecute() { //PROGRAMAR UN CIRCULO GIRANDO O ALGO ASÍ!!!!!!!!!

            dialog.setMessage("Cargando..");
            dialog.setTitle("Leyendo datos");
            dialog.setCancelable(false);
            dialog.setProgressStyle(dialog.STYLE_SPINNER);
            dialog.show();

        }

        @Override
        protected void onPostExecute(Void result) {

            nombre.setText("Nombre: " + e.getNombre());
            ci.setText("CI: " + e.getCedula());
            telefono.setText("Teléfono: " + e.getTelefono());
            mail.setText("E-mail: " + e.getMail());
            faltas.setText("Faltas: " + fal);

            new ImageLoadTask(e.getFoto(), img).execute();

            dialog.dismiss();
        }


        public class ImageLoadTask extends AsyncTask<Void, Void, Bitmap> {

            private String url;
            private ImageView imageView;

            public ImageLoadTask(String url, ImageView imageView) {
                this.url = url;
                this.imageView = imageView;
            }

            @Override
            protected Bitmap doInBackground(Void... params) {
                try {
                    URL urlConnection = new URL(url);
                    HttpURLConnection connection = (HttpURLConnection) urlConnection
                            .openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap myBitmap = BitmapFactory.decodeStream(input);
                    return myBitmap;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                super.onPostExecute(result);
                imageView.setImageBitmap(result);
            }

        }
    }
}
