package com.example.ussuario.tallerandroid.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ussuario.tallerandroid.Entidades.Comentario;
import com.example.ussuario.tallerandroid.NavigationDrawer;
import com.example.ussuario.tallerandroid.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetalleEjercicio.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DetalleEjercicio#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetalleEjercicio extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    int idEjer, idPractico;
    LinearLayout cont;
    ImageView img;
    Button boton;
    String imagen;
    List<Comentario> comentarios = new ArrayList();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public DetalleEjercicio() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetalleEjercicio.
     */
    // TODO: Rename and change types and number of parameters
    public static DetalleEjercicio newInstance(String param1, String param2) {
        DetalleEjercicio fragment = new DetalleEjercicio();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_detalle_ejercicio, container, false);
        cont = (LinearLayout) v.findViewById(R.id.contComentarios);
        img = (ImageView) v.findViewById(R.id.imgEjercicio);
        boton = (Button) v.findViewById(R.id.button);


        idEjer = 0;
        idPractico = 0;

        Bundle args = getArguments();
        if(args != null){
            idEjer = args.getInt("idEjercicio");
            idPractico = args.getInt("idPractico");
        }

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment agregarComentario = new AgregarComentario();

                Bundle args = new Bundle();
                args.putInt("idEjercicio", idEjer);
                args.putInt("idPractico", idPractico);
                args.putInt("ciEstudiante", NavigationDrawer.cedula);
                agregarComentario.setArguments(args);

                ((NavigationDrawer)getActivity()).CambiarFragment(agregarComentario);
            }
        });


        detalleEjercicio de = new detalleEjercicio();
        de.execute();

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class detalleEjercicio extends AsyncTask<Void, Void, Void> {
        ProgressDialog dialog = new ProgressDialog(getActivity());

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("DetalleEjercicio","doInBackground");
            HttpClient httpClient = new DefaultHttpClient();

            HttpGet get = new HttpGet("http://cristmathbri.webcindario.com/ListarEjercicios.php?IdPractico=" + idPractico);
            get.setHeader("Content-type", "application/json");

            try{
                HttpResponse resp = httpClient.execute(get);
                String respString = EntityUtils.toString(resp.getEntity());
                JSONObject jsonResultado = new JSONObject(respString);
                JSONArray respJSONa = jsonResultado.getJSONArray("Ejercicios");
                for(int i = 0; i < respJSONa.length(); i++){
                    JSONObject jsonObject = respJSONa.getJSONObject(i);

                    if(jsonObject.getInt("numero") == idEjer){ //Si el ejercicio es el elegido
                        imagen = jsonObject.getString("imagen"); //Guardamos la ruta de la imàgen
                        JSONArray comen = jsonObject.getJSONArray("comentarios"); //Traemos los comentarios
                        for(int j = 0; j < comen.length(); j++){
                            JSONObject comentario = comen.getJSONObject(j);

                            int ciEst = comentario.getInt("ciestudiante");
                            String fecha = comentario.getString("fecha");
                            String contenido = comentario.getString("contenido");

                            Comentario c = new Comentario(ciEst, fecha, contenido);
                            comentarios.add(c);

                        }

                    }
                }

            }
            catch(Exception ex){
                Log.e("ServicioRest","Error!", ex);
                ex.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPreExecute(){
            dialog.setMessage("Cargando..");
            dialog.setTitle("Leyendo datos");
            dialog.setCancelable(false);
            dialog.setProgressStyle(dialog.STYLE_SPINNER);
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void result){
            new ImageLoadTask(imagen, img).execute();

            Iterator<Comentario> i = comentarios.iterator();
            Comentario elem;
            while(i.hasNext()){
                elem=i.next();
                cargarComentario(elem);
            }
            dialog.dismiss();
        }

        private void cargarComentario(Comentario c){
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            param.setMargins(2,15,0,0);
            final TextView com = new TextView(getContext());
            com.setText("(" + c.getFecha() + ")- " + c.getCiEst() + " - " + c.getContenido());
            com.setLayoutParams(param);
            com.setTextSize(15);
            com.setTextColor(Color.parseColor("#000000"));

            cont.addView(com);
        }




        public class ImageLoadTask extends AsyncTask<Void, Void, Bitmap> {

            private String url;
            private ImageView imageView;

            public ImageLoadTask(String url, ImageView imageView) {
                this.url = url;
                this.imageView = imageView;
            }

            @Override
            protected Bitmap doInBackground(Void... params) {
                try {
                    URL urlConnection = new URL(url);
                    HttpURLConnection connection = (HttpURLConnection) urlConnection
                            .openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap myBitmap = BitmapFactory.decodeStream(input);
                    return myBitmap;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                super.onPostExecute(result);
                imageView.setImageBitmap(result);
            }

        }
    }
}
