package com.example.ussuario.tallerandroid.Entidades;

/**
 * Created by Usuario on 18/10/2016.
 */
public class Teorico {
    private int id;
    private int numero;
    private String pdf;

    public Teorico(){}

    public Teorico(int i, int n, String p){
        id =i;
        numero = n;
        pdf = p;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }
}
