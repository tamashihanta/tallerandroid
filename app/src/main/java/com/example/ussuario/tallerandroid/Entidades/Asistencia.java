package com.example.ussuario.tallerandroid.Entidades;

/**
 * Created by Usuario on 25/10/2016.
 */
public class Asistencia {
    private int idClase;
    private int ciEstudiante;
    private String nombre;
    private int telefono;
    private String mail;
    private String foto;

    public Asistencia(){}

    public Asistencia(int idClase, int ciEstudiante, String nombre, int telefono, String mail, String foto) {
        this.idClase = idClase;
        this.ciEstudiante = ciEstudiante;
        this.nombre = nombre;
        this.telefono = telefono;
        this.mail = mail;
        this.foto = foto;
    }

    public int getIdClase() {
        return idClase;
    }

    public void setIdClase(int idClase) {
        this.idClase = idClase;
    }

    public int getCiEstudiante() {
        return ciEstudiante;
    }

    public void setCiEstudiante(int ciEstudiante) {
        this.ciEstudiante = ciEstudiante;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
