package com.example.ussuario.tallerandroid.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ussuario.tallerandroid.NavigationDrawer;
import com.example.ussuario.tallerandroid.R;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AgregarComentario.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AgregarComentario#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AgregarComentario extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    EditText com;
    Button btnEnviar;
    int idPractico;
    int idEjercicio;
    int ciEstudiante;

    private OnFragmentInteractionListener mListener;

    public AgregarComentario() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AgregarComentario.
     */
    // TODO: Rename and change types and number of parameters
    public static AgregarComentario newInstance(String param1, String param2) {
        AgregarComentario fragment = new AgregarComentario();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View v = inflater.inflate(R.layout.fragment_agregar_comentario, container, false);


        com = (EditText) v.findViewById(R.id.comentario);
        btnEnviar = (Button) v.findViewById(R.id.button);


        idPractico = 0;
        idEjercicio = 0;
        ciEstudiante = 0;
        Bundle args = getArguments();
        if(args != null){
            idEjercicio = args.getInt("idEjercicio");
            idPractico = args.getInt("idPractico");
            ciEstudiante = args.getInt("ciEstudiante");
        }


        btnEnviar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                String comentario = com.getText().toString();

                Time now = new Time();
                now.setToNow();
                String fechaHora = now.format("%d-%m-%Y %H:%M");

                String cedula = String.valueOf(ciEstudiante);
                String idEj = String.valueOf(idEjercicio);
                String idPr = String.valueOf(idPractico);

                if (!comentario.equals("")) {

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost post = new HttpPost("http://cristmathbri.webcindario.com/CrearComentario.php");

                    try {
                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(5);
                        nameValuePairs.add(new BasicNameValuePair("ciestudiante", cedula));
                        nameValuePairs.add(new BasicNameValuePair("idpractico", idPr));
                        nameValuePairs.add(new BasicNameValuePair("idejercicio", idEj));
                        nameValuePairs.add(new BasicNameValuePair("fecha", fechaHora));
                        nameValuePairs.add(new BasicNameValuePair("contenido", comentario));
                        post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                        HttpResponse resp = httpClient.execute(post);

                        if (resp.getStatusLine().getStatusCode() == HttpsURLConnection.HTTP_OK) {
                            Toast.makeText(getActivity(), "Tu comentario fue ingresado con éxito", Toast.LENGTH_SHORT).show();

                            Fragment listarPracticos = new ListarPracticos();
                            ((NavigationDrawer)getActivity()).CambiarFragment(listarPracticos);
                        } else {
                            Toast.makeText(getActivity(), "Hubo un error en el servidor. Inténtalo de nuevo", Toast.LENGTH_SHORT).show();
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } else {
                    Toast.makeText(getActivity(), "No ha ingresado ningún comentario aún", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
