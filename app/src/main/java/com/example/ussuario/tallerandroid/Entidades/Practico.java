package com.example.ussuario.tallerandroid.Entidades;

/**
 * Created by Usuario on 18/10/2016.
 */
public class Practico {
    private int id;
    private int numero;

    public Practico(){}

    public Practico(int i, int n){
        id = i;
        numero = n;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
}
