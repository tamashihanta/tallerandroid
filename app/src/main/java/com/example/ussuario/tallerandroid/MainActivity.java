package com.example.ussuario.tallerandroid;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    public Map<String, String> params;

    EditText txtNick, txtPass;
    Button btnAceptar;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        //Controller.iniciar();

        txtNick = (EditText) findViewById(R.id.nick);
        txtPass = (EditText) findViewById(R.id.pass);


        btnAceptar = (Button) findViewById(R.id.btnAceptar);
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nick, pass;
                nick = txtNick.getText().toString();
                pass = txtPass.getText().toString();

                if (!nick.equals("") && !pass.equals("")) {

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost post = new HttpPost("http://cristmathbri.webcindario.com/Login.php");
                    //post.setHeader("content-type", "application/json");

                    try {
                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                        nameValuePairs.add(new BasicNameValuePair("cedula", nick));
                        nameValuePairs.add(new BasicNameValuePair("password", pass));
                        post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                        HttpResponse resp = httpClient.execute(post);

                        if (resp.getStatusLine().getStatusCode() == HttpsURLConnection.HTTP_OK) {
                            Intent nuevoForm = new Intent(MainActivity.this, NavigationDrawer.class);
                            nuevoForm.putExtra("ci", nick);
                            startActivity(nuevoForm);
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), "Datos incorrectos. Inténtelo nuevamente.", Toast.LENGTH_SHORT).show();
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } else {
                    Toast.makeText(getApplicationContext(), "Por favor ingrese los datos", Toast.LENGTH_SHORT).show();
                }
            }
        });


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }
}
