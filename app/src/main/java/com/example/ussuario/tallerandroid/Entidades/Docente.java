package com.example.ussuario.tallerandroid.Entidades;

/**
 * Created by ussuario on 27/09/2016.
 */
public class Docente {
    private int id;
    private String nombre;
    private String nick;
    private String pass;

    public Docente(){}

    public Docente(int i, String n, String ni, String p){
        this.id = i;
        this.nombre = n;
        this.nick = ni;
        this.pass = p;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
