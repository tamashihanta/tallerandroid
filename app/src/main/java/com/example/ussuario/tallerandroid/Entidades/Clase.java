package com.example.ussuario.tallerandroid.Entidades;

/**
 * Created by Usuario on 18/10/2016.
 */
public class Clase {
    private int id;
    private String fecha;
    private String tema;

    public Clase(){}

    public Clase (int i, String f, String t){
        id=i;
        fecha=f;
        tema=t;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }
}
