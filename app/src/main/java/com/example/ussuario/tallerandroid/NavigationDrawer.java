package com.example.ussuario.tallerandroid;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.ussuario.tallerandroid.Fragments.AgregarComentario;
import com.example.ussuario.tallerandroid.Fragments.Asistencias;
import com.example.ussuario.tallerandroid.Fragments.CerrarSesion;
import com.example.ussuario.tallerandroid.Fragments.Configurar;
import com.example.ussuario.tallerandroid.Fragments.DetalleEjercicio;
import com.example.ussuario.tallerandroid.Fragments.DetalleEstudiante;
import com.example.ussuario.tallerandroid.Fragments.ListarClases;
import com.example.ussuario.tallerandroid.Fragments.ListarEjercicios;
import com.example.ussuario.tallerandroid.Fragments.ListarEstudiantes;
import com.example.ussuario.tallerandroid.Fragments.ListarPracticos;
import com.example.ussuario.tallerandroid.Fragments.ListarTeoricos;
import com.example.ussuario.tallerandroid.Fragments.Perfil;

public class NavigationDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ListarClases.OnFragmentInteractionListener, ListarEstudiantes.OnFragmentInteractionListener, ListarTeoricos.OnFragmentInteractionListener, ListarPracticos.OnFragmentInteractionListener, Configurar.OnFragmentInteractionListener, Asistencias.OnFragmentInteractionListener,
            DetalleEstudiante.OnFragmentInteractionListener, ListarEjercicios.OnFragmentInteractionListener, DetalleEjercicio.OnFragmentInteractionListener, CerrarSesion.OnFragmentInteractionListener, AgregarComentario.OnFragmentInteractionListener, Perfil.OnFragmentInteractionListener {

    public static int cedula;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle param = getIntent().getExtras();
        if(param != null){
            NavigationDrawer.cedula = Integer.valueOf(param.getString("ci"));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //titulo para el navigation drawer
        getSupportActionBar().setTitle("Taller Android");

        Fragment perfil = new Perfil();
        getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, perfil).commit();
    }

    @Override
    public void onBackPressed() {
        /*
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        /*
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        */

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment fragment = null;
        Boolean seleccionado = false;
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (id == R.id.ListEstudiantes) {
            fragment = new ListarEstudiantes();
            seleccionado = true;
        } else if (id == R.id.ListClases) {
            fragment = new ListarClases();
            seleccionado = true;
        } else if (id == R.id.ListTeoricos) {
            fragment = new ListarTeoricos();
            seleccionado = true;
        } else if (id == R.id.ListPracticos) {
            fragment = new ListarPracticos();
            seleccionado = true;
        } else if (id == R.id.Perfil) {
            fragment = new Perfil();
            seleccionado = true;
        } else if (id == R.id.CerrarSesion) {
            Intent nuevoForm = new Intent(NavigationDrawer.this, MainActivity.class);
            startActivity(nuevoForm);
            finish();
        }

        if(seleccionado){
            getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, fragment).commit();
        }


        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void CambiarFragment(Fragment f){
        getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, f).commit();
    }
}
