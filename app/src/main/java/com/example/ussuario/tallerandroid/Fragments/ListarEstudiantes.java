package com.example.ussuario.tallerandroid.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ussuario.tallerandroid.Entidades.Estudiante;
import com.example.ussuario.tallerandroid.NavigationDrawer;
import com.example.ussuario.tallerandroid.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListarEstudiantes.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListarEstudiantes#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListarEstudiantes extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    List<Estudiante> estudiantes = new ArrayList();
    LinearLayout linear;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ListarEstudiantes() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListarEstudiantes.
     */
    // TODO: Rename and change types and number of parameters
    public static ListarEstudiantes newInstance(String param1, String param2) {
        ListarEstudiantes fragment = new ListarEstudiantes();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private void cargarEst(Estudiante e) {
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        param.setMargins(2,5,0,0);
        final TextView estudiante = new TextView(this.getContext());
        estudiante.setId(e.getCedula());
        estudiante.setText(e.getNombre());
        estudiante.setLayoutParams(param);
        estudiante.setTextSize(30);
        estudiante.setTextColor(Color.parseColor("#000000"));
        estudiante.setGravity(Gravity.CENTER);

        estudiante.setClickable(true);

        estudiante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment detalle = new DetalleEstudiante();
                Bundle args = new Bundle();
                args.putInt("ciEstudiante", estudiante.getId());
                detalle.setArguments(args);
                ((NavigationDrawer)getActivity()).CambiarFragment(detalle);
            }
        });

        linear.addView(estudiante);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_listar_estudiantes, container, false);
        linear = (LinearLayout) v.findViewById(R.id.EstudiantesAca);

        listarEstudiantes le = new listarEstudiantes();
        le.execute();

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class listarEstudiantes extends AsyncTask<Void, Void, Void> {
        ProgressDialog dialog = new ProgressDialog(getActivity());

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TraerEstudiantes","doInBackground");
            HttpClient httpClient = new DefaultHttpClient();

            HttpGet get = new HttpGet("http://cristmathbri.webcindario.com/ListarEstudiantes.php");
            get.setHeader("Content-type", "application/json");

            try{
                HttpResponse resp = httpClient.execute(get);
                String respString = EntityUtils.toString(resp.getEntity());
                JSONObject jsonResultado = new JSONObject(respString);
                JSONArray respJSONa = jsonResultado.getJSONArray("Estudiantes");
                for(int i = 0; i < respJSONa.length(); i++){
                    JSONObject jsonObject = respJSONa.getJSONObject(i);

                    int cedula = jsonObject.getInt("cedula");
                    String nombre = jsonObject.getString("nombre");
                    int telefono = jsonObject.getInt("telefono");
                    String mail = jsonObject.getString("mail");
                    String foto = jsonObject.getString("foto");
                    Estudiante e = new Estudiante(cedula, nombre, telefono, mail, foto);

                    estudiantes.add(e);
                }

            }
            catch(Exception ex){
                Log.e("ServicioRest","Error!", ex);
                ex.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute(){

            dialog.setMessage("Cargando..");
            dialog.setTitle("Leyendo datos");
            dialog.setCancelable(false);
            dialog.setProgressStyle(dialog.STYLE_SPINNER);
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void result){
            Iterator<Estudiante> i = estudiantes.iterator();
            Estudiante elem;
            while(i.hasNext()){
                elem=i.next();
                cargarEst(elem);
            }

            dialog.dismiss();
        }
    }
}
