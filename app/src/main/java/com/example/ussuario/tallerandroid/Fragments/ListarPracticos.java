package com.example.ussuario.tallerandroid.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ussuario.tallerandroid.Entidades.Practico;
import com.example.ussuario.tallerandroid.NavigationDrawer;
import com.example.ussuario.tallerandroid.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListarPracticos.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListarPracticos#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListarPracticos extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private LinearLayout cont;
    private List<Practico> practicos = new ArrayList();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ListarPracticos() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListarPracticos.
     */
    // TODO: Rename and change types and number of parameters
    public static ListarPracticos newInstance(String param1, String param2) {
        ListarPracticos fragment = new ListarPracticos();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_listar_practicos, container, false);
        cont = (LinearLayout) v.findViewById(R.id.contPracticos);
        listarPracticos lp = new listarPracticos();
        lp.execute();
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class listarPracticos extends AsyncTask<Void, Void, Void> {
        ProgressDialog dialog = new ProgressDialog(getActivity());

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TraerPracticos","doInBackground");
            HttpClient httpClient = new DefaultHttpClient();

            HttpGet get = new HttpGet("http://cristmathbri.webcindario.com/ListarPracticos.php");
            get.setHeader("Content-type", "application/json");

            try{
                HttpResponse resp = httpClient.execute(get);
                String respString = EntityUtils.toString(resp.getEntity());
                JSONObject jsonResultado = new JSONObject(respString);
                JSONArray respJSONa = jsonResultado.getJSONArray("Practicos");
                for(int i = 0; i < respJSONa.length(); i++){
                    JSONObject jsonObject = respJSONa.getJSONObject(i);

                    int id = jsonObject.getInt("id");
                    int numero = jsonObject.getInt("numero");
                    Practico p = new Practico(id, numero);

                    practicos.add(p);
                }

            }
            catch(Exception ex){
                Log.e("ServicioRest","Error!", ex);
                ex.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPreExecute(){
            dialog.setMessage("Cargando..");
            dialog.setTitle("Leyendo datos");
            dialog.setCancelable(false);
            dialog.setProgressStyle(dialog.STYLE_SPINNER);
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void result){
            Iterator<Practico> i = practicos.iterator();
            Practico elem;
            while(i.hasNext()){
                elem=i.next();
                cargarTeorico(elem);
            }
            dialog.dismiss();
        }

        private void cargarTeorico(Practico p){
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            param.setMargins(2,5,0,0);
            final TextView practico = new TextView(getContext());
            practico.setId(p.getId());
            practico.setText("Practico " + p.getNumero());
            practico.setLayoutParams(param);
            practico.setTextSize(30);
            practico.setTextColor(Color.parseColor("#000000"));
            practico.setGravity(Gravity.CENTER);

            practico.setClickable(true);

            practico.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment ejer = new ListarEjercicios();
                    Bundle args = new Bundle();
                    args.putInt("idPractico", practico.getId());
                    ejer.setArguments(args);
                    ((NavigationDrawer)getActivity()).CambiarFragment(ejer);
                }
            });

            cont.addView(practico);
        }
    }
}
