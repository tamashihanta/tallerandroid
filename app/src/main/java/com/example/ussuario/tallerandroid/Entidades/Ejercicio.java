package com.example.ussuario.tallerandroid.Entidades;

/**
 * Created by Usuario on 25/10/2016.
 */
public class Ejercicio {
    private int id;
    private int idPractico;
    private String imagen;

    public Ejercicio(){}

    public Ejercicio(int id, int idPractico, String imagen) {
        this.id = id;
        this.idPractico = idPractico;
        this.imagen = imagen;
    }

    public Ejercicio(int id){
        this.id = id;
        this.idPractico = 0;
        this.imagen = "NO IMAGE";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdPractico() {
        return idPractico;
    }

    public void setIdPractico(int idPractico) {
        this.idPractico = idPractico;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
