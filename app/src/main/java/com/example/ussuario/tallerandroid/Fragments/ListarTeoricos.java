package com.example.ussuario.tallerandroid.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ussuario.tallerandroid.Entidades.Teorico;
import com.example.ussuario.tallerandroid.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListarTeoricos.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListarTeoricos#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListarTeoricos extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private LinearLayout cont;
    List<Teorico> teoricos = new ArrayList();


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ListarTeoricos() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListarTeoricos.
     */
    // TODO: Rename and change types and number of parameters
    public static ListarTeoricos newInstance(String param1, String param2) {
        ListarTeoricos fragment = new ListarTeoricos();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_listar_teoricos, container, false);
        cont = (LinearLayout) v.findViewById(R.id.contTeoricos);
        listarTeoricos lt = new listarTeoricos();
        lt.execute();
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class listarTeoricos extends AsyncTask<Void, Void, Void> {
        ProgressDialog dialog = new ProgressDialog(getActivity());

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TraerTeoricos","doInBackground");
            HttpClient httpClient = new DefaultHttpClient();

            HttpGet get = new HttpGet("http://cristmathbri.webcindario.com/ListarTeoricos.php");
            get.setHeader("Content-type", "application/json");

            try{
                HttpResponse resp = httpClient.execute(get);
                String respString = EntityUtils.toString(resp.getEntity());
                JSONObject jsonResultado = new JSONObject(respString);
                JSONArray respJSONa = jsonResultado.getJSONArray("Teoricos");
                for(int i = 0; i < respJSONa.length(); i++){
                    JSONObject jsonObject = respJSONa.getJSONObject(i);

                    int id = jsonObject.getInt("id");
                    int numero = jsonObject.getInt("numero");
                    String pdf = jsonObject.getString("pdf");
                    Teorico t = new Teorico(id, numero, pdf);

                    teoricos.add(t);
                }

            }
            catch(Exception ex){
                Log.e("ServicioRest","Error!", ex);
                ex.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPreExecute(){
            dialog.setMessage("Cargando..");
            dialog.setTitle("Leyendo datos");
            dialog.setCancelable(false);
            dialog.setProgressStyle(dialog.STYLE_SPINNER);
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void result){
            Iterator<Teorico> i = teoricos.iterator();
            Teorico elem;
            while(i.hasNext()){
                elem=i.next();
                cargarTeorico(elem);
            }
            dialog.dismiss();
        }

        private void cargarTeorico(Teorico t){
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            param.setMargins(2,5,0,0);
            final TextView teorico = new TextView(getContext());
            teorico.setId(t.getId());
            teorico.setText("Teorico " + t.getNumero());
            teorico.setLayoutParams(param);

            teorico.setTextSize(30);
            teorico.setTextColor(Color.parseColor("#000000"));
            teorico.setGravity(Gravity.CENTER);

            cont.addView(teorico);
        }
    }
}
