package com.example.ussuario.tallerandroid.Entidades;

/**
 * Created by Usuario on 25/10/2016.
 */
public class Comentario {
    private int ciEst;
    private String fecha;
    private String contenido;

    public Comentario(){}

    public Comentario(int ciEst, String fecha, String contenido) {
        this.ciEst = ciEst;
        this.fecha = fecha;
        this.contenido = contenido;
    }

    public int getCiEst() {
        return ciEst;
    }

    public void setCiEst(int ciEst) {
        this.ciEst = ciEst;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }
}
