package com.example.ussuario.tallerandroid.Entidades;

/**
 * Created by ussuario on 27/09/2016.
 */
public class Estudiante {
    private int cedula;
    private String nombre;
    private int telefono;
    private String mail;
    private String foto;

    public Estudiante(){}

    public Estudiante(int c, String n, int t, String m, String f){
        cedula = c;
        nombre = n;
        telefono = t;
        mail = m;
        foto = f;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
